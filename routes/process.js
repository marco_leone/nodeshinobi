var express = require('express');
var router = express.Router();
var cp = require('child_process');


/* GET home page. */
router.post('/getMonitorProcessInfo', function(req, res, next) { //path: process/getMonitorProcessInfo
    /*
     * /dev\/shm\/: matches string beginning with "dev/shm/streams/"
     * (.*?) : matches any character
     * \s:	   space
     */
    var pattern = /dev\/shm\/streams(.*?)\s/;
    //TODO: cmd deve essere aggiornato per essere eguito su macchina remota
    //NB: simbolo - davanti a [%ORDERBY%] per ordine decrescente;
    //grep -ie "[f]fmpeg": permette di escludere grep dall'elenco dei processi restituiti
    var cmd = 'ps -eo pid,user,pcpu,args --sort -[%ORDERBY%] | grep -ie "[f]fmpeg" | head -n 1', param;

    switch (req.body.orderBy) {
        case 'cpu':
            param = 'pcpu';
            break;
    }

    cmd = cmd.replace('[%ORDERBY%]', param);

    //ottieni mid (Monitor ID) del processo che sta consumando più risorse
    let stdout = cp.execSync(cmd).toString();
    var result = pattern.exec(stdout)[1].split("/");

    if(result[1].length === 0 || result[2].length === 0) {
        res.json(JSON.stringify({}));
    }
    else {
        res.json(JSON.stringify({ke:result[1], mid:result[2]}));
    }

});

module.exports = router;
